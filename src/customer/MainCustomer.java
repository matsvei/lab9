package customer;

import customer.Customer;

import java.util.List;

public class MainCustomer {
    public static void main(String[] args) {
        Shop megamarket = new Shop("Megamarket");
        megamarket.addCustomerToShopList(new Customer(1337, "Иван", "Дроздов", "Михайлович", "Украина", 11_22_33_44, 12345));
        megamarket.addCustomerToShopList(new Customer(8624, "Андрей", "Фамильян", "Иванович", "Россия", 22_33_44_55, 23456));
        megamarket.addCustomerToShopList(new Customer(2005, "Богдан", "Мурахоедов", "Франкович", "Болгария", 33445566, 34567));
        megamarket.addCustomerToShopList(new Customer(6006,"Виктор", "Викторян", "Викторович", "Украина", 44_55_66_77, 45678));
        megamarket.addCustomerToShopList(new Customer(11313, "Франк", "Ибрагимов", "Ибрагимович", "Латвия", 55_66_77_88, 56789));
        megamarket.addCustomerToShopList(new Customer(1111, "Наташа", "Дроздова", "Алексеевна", "Россия", 66_77_88_99, 67890));
        megamarket.addCustomerToShopList(new Customer(123,"Оксана", "Ростинова", "Ростиславовна", "Украина", 77_88_99_00, 78901));
        megamarket.addCustomerToShopList(new Customer(1234, "Степан", "Боруков", "Алексеевич", "Россия", 88_99_00_11, 89012));
        megamarket.addCustomerToShopList(new Customer(99999,"Steeve", "Jackson", "<noname>", "USA", 99_00_11_22, 90123));
        megamarket.addCustomerToShopList(new Customer(3339, "Александр", "Добрынин", "Станиславович", "Украина", 10_12_23_34, 10234));

        System.out.println("\nsort by name:");
        List<Customer> listSortByName = megamarket.getListByName();
        for (Customer customer : listSortByName) {
            System.out.println(customer);
        }



        System.out.println("\nprint customers by diapason credit card:");
        int diapasonStart = 22_33_44_55;
        int diapasonEnd = 66_77_88_99;
        List<Customer> listSortByDiapasonCreditCard = megamarket.getListByDiapasonCreditCard(diapasonStart, diapasonEnd);
        for (Customer customer : listSortByDiapasonCreditCard) {
            System.out.println(customer);
        }


    }
}
