package customer;

import customer.Customer;

import java.util.ArrayList;
import java.util.List;

class Shop {
    final String shopName;
    public final ArrayList<Customer> customersList = new ArrayList<>();

    Shop(String shopName) {
        this.shopName = shopName;
    }

    void addCustomerToShopList(Customer customer) {
        customersList.add(customer);
    }

    List<Customer> getListByName() {
        List<Customer> list = new ArrayList<>(customersList);
        list.sort((object1, object2) -> object1.getName().compareToIgnoreCase(object2.getName()));
        return list;
    }



    List<Customer> getListByDiapasonCreditCard(int diapasonStart, int diapasonEnd) {
        List<Customer> list = new ArrayList<>();
        for (Customer customer : customersList) {
            if (customer.getCreditCardId() >= diapasonStart && customer.getCreditCardId() <=diapasonEnd) {
                list.add(customer);
            }
        }

        return list;
    }



}
