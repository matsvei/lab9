package customer;

class Customer {
    final int id;
    final String name;
    final String name2;
    final String name3;
    final String address;
    final int creditCardId;
    final int bankNumberCard;

    Customer(int id, String name, String name2, String name3, String address, int creditCardId, int bankNumberCard) {
        this.id = id;
        this.name = name;
        this.name2 = name2;
        this.name3 = name3;
        this.address = address;
        this.creditCardId = creditCardId;
        this.bankNumberCard = bankNumberCard;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getName2() {
        return name2;
    }

    public String getName3() {
        return name3;
    }

    public String getAddress() {
        return address;
    }

    public int getCreditCardId() {
        return creditCardId;
    }

    public int getBankNumberCard() {
        return bankNumberCard;
    }

    public String toString() {
        return String.format("ID: %d\t name: %s\t name2: %s\t name3 %s \t from: %s\t credit card %d\t bank number %d",
                id, name, name2, name3, address, creditCardId, bankNumberCard);
    }

}
