package student;

class Students {
    final String name;
    final String initials;
    final int group;
    private int[] grades;
    private int numberGrades;
    public Students(String name, String initials, int group) {
        this.name = name;
        this.initials = initials;
        numberGrades = 0;
        grades = new int[5];
        this.group = group;
    }
    boolean checkGrade(int grade) {
        if (numberGrades >= 5) {
            return false;
        }
        grades[numberGrades] = grade;
        numberGrades++;
        return true;
    }
    boolean isGood() {
        for (int i = 0; i < numberGrades; ++i) {
            if (grades[i] != 9 && grades[i] != 10) {
                return false;
            }
        }
        return true;
    }
    void print() {
        System.out.print(name + " " + initials);
        System.out.print(" Group: " + group + ". Grades: ");
        for (int i = 0; i < numberGrades; ++i)
            System.out.print(grades[i] + " ");
        System.out.print("\n");
    }

    public String getName() {
        return name;
    }

    public String getInitials() {
        return initials;
    }

    public int getGroup() {
        return group;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    public int getNumberGradesGrades() {
        return numberGrades;
    }

    public void setNumberGrades(int numberGrades) {
        this.numberGrades = numberGrades;
    }
}