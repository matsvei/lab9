package student;

import java.util.Random;

public class MainStudents {
    public static void main(String[] args) {
        int n = 10;
        Students students[] = new Students[n];
        students[0] = new Students("Petrov", "A.I.", 1);
        students[1] = new Students("Petrov", "B.I.", 1);
        students[2] = new Students("Ivanov", "A.S..", 1);
        students[3] = new Students("Sidorov", "A.S.", 2);
        students[4] = new Students("Akulin", "K.M.", 2);
        students[5] = new Students("Sergeev", "I.I.", 3);
        students[6] = new Students("Fedosov", "I.K.", 3);
        students[7] = new Students("Mala", "V.V.", 3);
        students[8] = new Students("Ferk", "M.T.", 3);
        students[9] = new Students("Krotov", "F.G.", 3);
        Random rnd = new Random();
        for (int i = 0; i < n; ++i) {
            students[i].checkGrade(rnd.nextInt(6) + 5);
            students[i].checkGrade(rnd.nextInt(6) + 5);
            students[i].checkGrade(rnd.nextInt(6) + 5);
            students[i].checkGrade(rnd.nextInt(6) + 5);
        }
        System.out.println("все студенты: ");
        for (int i = 0; i < n; ++i) {
            students[i].print();
        }
        System.out.println("отличники: ");
        for (int i = 0; i < n; ++i) {
            if (students[i].isGood())
                students[i].print();
        }
    }
}

