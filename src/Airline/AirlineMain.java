package Airline;


import java.util.List;

public class AirlineMain {
    public static void main(String[] args) {
        Airport timetable = new Airport("Closet");
        timetable.addAirlineToAirlineList(new Airline("USA", 24794, "airplane", "16:50", "Monday"));
        timetable.addAirlineToAirlineList(new Airline("Greece", 93294, "airship", "14:30", "Tuesday"));
        timetable.addAirlineToAirlineList(new Airline("France", 34957, "balloon", "06:15", "Wednesday"));
        timetable.addAirlineToAirlineList(new Airline("Italy", 52932, "airplane", "15:00", "Thursday"));
        timetable.addAirlineToAirlineList(new Airline("Germany", 33974, "airplane", "12:30", "Friday"));
        timetable.addAirlineToAirlineList(new Airline("Lithuania", 52374, "airship", "07:30", "Monday"));
        timetable.addAirlineToAirlineList(new Airline("England", 13371, "airplane", "08:40", "Thursday"));
        timetable.addAirlineToAirlineList(new Airline("Estonia", 53287, "balloon", "14:50", "Friday"));
        timetable.addAirlineToAirlineList(new Airline("Latvia", 63247, "airplane", "13:15", "Tuesday"));
        timetable.addAirlineToAirlineList(new Airline("Spain", 87234, "airship", "10:20", "Monday"));

        System.out.println("\nprint airlines by Destination :");
        String country = "France";
        List<Airline> listSortByDestination = timetable.getListByDestination(country);
        for (Airline airline : listSortByDestination) {
            System.out.println(airline);
        }

        System.out.println("\nprint Airlines by Day :");
        String day = "Monday";
        List<Airline> listSortByDay = timetable.getListByDay(day);
        for (Airline airline : listSortByDay) {
            System.out.println(airline);
        }
        String time = "12:00";
        System.out.println("\nprint Airlines by Day :");
        List<Airline> listSortByFlights = timetable.getListByFlights(day, time);
        for (Airline airline : listSortByFlights) {
            System.out.println(airline);
        }
    }
}
