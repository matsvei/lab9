package Airline;

class Airline {
    final String destination;
    final int number;
    final String type;
    final String time;
    final String day;
    Airline(String destination, int number, String type, String time, String day) {
        this.destination = destination;
        this.number = number;
        this.type = type;
        this.time = time;
        this.day = day;
    }
    public String getDestination() {
        return destination;
    }

    public int getNumber() {
        return number;
    }

    public String getType() {
        return type;
    }

    public String getTime() {
        return time;
    }

    public String getDay() {
        return day;
    }


    public String toString() {
        return String.format("destination: %s\t number: %d\t type: %s\t time: %s \t day: %s",
                destination, number, type, time, day);
    }

}