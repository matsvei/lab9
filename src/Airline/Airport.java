package Airline;

import java.util.ArrayList;
import java.util.List;

class Airport {
    final String airportName;
    public final ArrayList<Airline> airlinesList = new ArrayList<>();

    Airport(String airportName) {
        this.airportName = airportName;
    }

    void addAirlineToAirlineList(Airline airline) {
        airlinesList.add(airline);
    }

    List<Airline> getListByDestination(String destination) {
        List<Airline> list = new ArrayList<>();
        for (Airline airline : airlinesList) {
            if (airline.getDestination().equals(destination)) {
                list.add(airline);
            }
        }
        return list;
    }

    List<Airline> getListByDay(String day) {
        List<Airline> list = new ArrayList<>();
        for (Airline airline : airlinesList) {
            if (airline.getDay().equals(day)) {
                list.add(airline);
            }
        }
        return list;
    }

    List<Airline> getListByFlights(String day, String time) {
        List<Airline> list = new ArrayList<>();
        for (Airline airline : airlinesList) {
            if (airline.getDay().equals(day) && time.compareTo(airline.getTime()) < 0) {
                list.add(airline);
            }
        }

        return list;
    }
}

