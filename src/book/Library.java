package book;

import book.Book;

import java.util.*;


class Library {
    final String libraryName;
    public final ArrayList<Book> booksList = new ArrayList<>();
    Library(String libraryName) {
        this.libraryName = libraryName;
    }
    void addBookToBookList(Book book) {
        booksList.add(book);
    }
    List<Book> getListByAuthor(String author) {
        List<Book> list = new ArrayList<>();
        for (Book book : booksList) {
            if (book.getAuthor().equals(author)) {
                list.add(book);
            }
        }
        return list;
    }
    List<Book> getListByPublisher(String publisher) {
        List<Book> list = new ArrayList<>();
        for (Book book : booksList) {
            if (book.getPublisher().equals(publisher)) {
                list.add(book);
            }
        }
        return list;
    }
    List<Book> getListByYear(int year) {
        List<Book> list = new ArrayList<>();
        for (Book book : booksList) {
            if (book.getYear() >= year) {
                list.add(book);
            }
        }
        return list;
    }
}


