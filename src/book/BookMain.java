package book;
import java.util.List;

public class BookMain{
    public static void main(String[] args) {
        Library closet = new Library("Closet");
        closet.addBookToBookList(new Book(12, "Гарри Поттер", "Джоан Роулинг", "Hzkto", 1997, 500, 29, "soft board"));
        closet.addBookToBookList(new Book(1, "Идиот", "Федор Достоевский", "OldBooks", 2001, 430, 19, "hard board"));
        closet.addBookToBookList(new Book(102, "Критика чистого разума", "Иммануил Кант", "PhilosophyBooks", 2001, 480, 32, "hard board"));
        closet.addBookToBookList(new Book(1111,"Преступление и наказание", "Федор Достоевский", "OldBooks", 1969, 149, 13, "soft board"));
        closet.addBookToBookList(new Book(228, "Хоббит", "Джон Толкин", "FantasyBooks", 2012, 158, 15, "soft board"));

        System.out.println("\nprint book.Book by Author :");
        String author = "Федор Достоевский";
        List<Book> listSortByAuthor = closet.getListByAuthor(author);
        for (Book book : listSortByAuthor) {
            System.out.println(book);
        }
        String publisher = "OldBooks";
        System.out.println("\nprint book by publisher :");
        List<Book> listSortByPublisher = closet.getListByPublisher(publisher);
        for (Book book : listSortByPublisher) {
            System.out.println(book);
        }
        System.out.println("\nprint book by Year :");
        List<Book> listSortByYear = closet.getListByYear(2000);
        for (Book book : listSortByYear) {
            System.out.println(book);
        }
    }
}
