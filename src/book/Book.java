package book;

class Book {
    final int id;
    final String name;
    final String author;
    final String publisher;
    final int year;
    final int pages;
    final int price;
    final String binding;
    Book(int id, String name, String author, String publisher, int year, int pages, int price, String binding) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.publisher = publisher;
        this.year = year;
        this.pages = pages;
        this.price = price;
        this.binding = binding;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getPublisher() {
        return publisher;
    }

    public int getYear() {
        return year;
    }

    public int getPages() {
        return pages;
    }

    public int getPrice() {
        return price;
    }

    public String getBinding() {
        return binding;
    }
    public String toString() {
        return String.format("ID: %d\t name: %s\t author: %s\t publisher: %s \t year: %d\t pages:  %d\t price: %d\t binding: %s",
                id, name, author, publisher, year, pages, price, binding);
    }

}