package trains;

class Train {
    final String destination;
    final int number;
    final String departureTime;

    Train(String destination, int number,  String departureTime) {
        this.destination = destination;
        this.number = number;
        this.departureTime = departureTime;

    }
    public String getDestination() {
        return destination;
    }

    public int getNumber() {
        return number;
    }

    public String getTime() {
        return departureTime;
    }

    public String toString() {
        return String.format("destination: %s\t number: %d\t  time: %s ",
                destination, number,  departureTime);
    }

}